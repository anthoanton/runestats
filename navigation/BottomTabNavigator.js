import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import Api from '../screens/api';
import {View,Text, Header, Image, Button} from 'react-native';
const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  //navigation.setOptions({ headerTitle: getHeaderTitle(route) });

   navigation.setOptions({
    headerTitle: getHeaderTitle(route),
    headerRight: () => (
      <Text 
        onPress={() => navigation.reset({
          index: 0,
          routes: [{ name: 'Home' }],
        })}
        style={{marginRight: 20}}
        >
        <TabBarIcon 
          name="md-home"/>
      </Text>

    ),
  });

  return (


    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="Home"
        component={Api}
        options={{
          title: 'Item Prices',
          tabBarIcon: ({ focused }) => 
          <Image
            style={{width: 45, height: 45}}
            source={require('../assets/images/product_oldschoolgold-500x500.png')}
          />
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName){
    case 'Home':
      return 'Item Prices';
  }
}
