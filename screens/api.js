import * as React from 'react';
import { 
Image, 
Platform, 
StyleSheet, 
Text, 
TouchableOpacity, 
View, 
FlatList, 
ActivityIndicator, 
TextInput, 
Button,
SectionList} from 'react-native';
import Constants from 'expo-constants';
import {number_format} from '../components/Utils.js'

export default class api extends React.Component {

  constructor(props){
    super(props);
    this.state ={ 
      HideInput:false,
    	isLoading: true,
    	URLBase:"http://api.runelite.net/runelite-1.6.6.1",
    	query: ""
    }
  }

  componentDidMount(){

    this.setState({
          isLoading: false,
          query:'',
          dataSource:''
           }, function(){

    });
  }

  async query(){

      this.setState({
          isLoading: true
           }, function(){

      });

      try {
        let response = await fetch(
          this.state.URLBase+'/item/search?query='+this.state.query,
        );
        let responseJson = await response.json();

        if (responseJson.items.length>0) {
          this.setState({
            isLoading: false,
            dataSource: responseJson.items,
          }, function(){

          }) 
        }else{
          this.setState({
            isLoading: false,
            HideInput:false,
            query:"",

          }, function(){

          }) 
        }

      } catch (error) {
        console.error(error);
      }

  }

  async GE(id){
    try {

        let dataArray = []

        let price = await fetch(
          this.state.URLBase+'/item/price?id='+id,
        );
        let responsePrice = await price.json();

        let average = await fetch(
          this.state.URLBase+'/osb/ge?itemId='+id,
        );
        let responseAverage = await average.json();


        dataArray.push({
          id:responsePrice[0].id,
          name:responsePrice[0].name,
          price:responsePrice[0].price,
          buy_average:responseAverage.buy_average,
          sell_average:responseAverage.sell_average,
          overall_average:responseAverage.overall_average
        })

        this.setState({
          isLoading: false,
          dataSource: dataArray,
        }, function(){

        })

      } catch (error) {
        console.error(error);
      }

  }




  format(number){
    var result = number_format(number, 0, ',', '.');;
    return result;
  }

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 40}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      
      <View style={styles.container}>
        {
          !this.state.HideInput?
          <View>
            <TextInput
              style={styles.textInput}
              autoFocus= {true}
              onChangeText={text => this.setState({query: text})}
              value={this.state.query}
            />
            <Button
              title="Search"
              color="#dddddd"
              onPress={() => {

                this.setState({
                  HideInput: true,
                }, function(){ })

                this.query()}}
            />
          </View>:null
        }
          <FlatList
            data={this.state.dataSource}
            renderItem={
              ({item}) =>
              <TouchableOpacity 
              style={styles.touch}
              onPress={() => {
                if (item.description){
                  this.GE(item.id)  
                }else{
                  this.setState({
                    isLoading: false,
                    HideInput:false,
                    query:"",
                  })
                }
              }}>

                <View style={styles.item}>
                  {
                    !item.price?
                    <Image
                    style={{width: 60, height: 60}}
                    source={{uri: this.state.URLBase+'/item/'+item.id+'/icon/large'}}
                  />:
                  <Image
                    style={{width: 80, height: 80}}
                    source={{uri: this.state.URLBase+'/item/'+item.id+'/icon/large'}}
                  />
                  }
                  
                  <Text style={styles.title}>{item.name}</Text>

                  {
                    !item.description?
                    <View>
                      <Text style={styles.price}>Price: {this.format(item.price) } gp</Text>
                      <Text style={styles.price}>Buy Average: {this.format(item.buy_average) } gp</Text>
                      <Text style={styles.price}>Sell Average: {this.format(item.sell_average) } gp</Text>
                      <Text style={styles.price}>Overall Average: {this.format(item.overall_average) } gp</Text>
                    </View>:
                    <Text>Description: {item.description}</Text>
                  }
                  
                </View>            
              </TouchableOpacity>
            }
            keyExtractor={({id}, index) => id}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    marginHorizontal: 16,
  },
  item: {
    backgroundColor: '#ffffff',
    padding: 10,
    marginVertical: 8,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center' 
  },
  header: {
    fontSize: 32,
  },
  title: {
    fontSize: 24,
    borderColor: '#ffffff'
  },
  price: {
    fontSize: 20,
  },
  touch: {
    overflow: 'hidden',
    borderRadius: 10
  },
  textInput: {
    justifyContent: "center",
    borderBottomWidth: 1,
    height: 50,
    borderColor: "#dddddd",
    marginVertical: 10
  }
});